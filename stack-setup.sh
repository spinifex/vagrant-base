#!/usr/bin/env bash

source "/vagrant/bootstrap-lib.sh"

# nginx install and configuration for a rails app
install nginx


if [ -e /etc/nginx/sites-available ]; then
  sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.old
  echo "Copied previous config to /etc/nginx/sites-available/default.old"
fi

echo "Updating default nginx config"

NGINX_CONFIG=$(cat << 'EOF'
upstream unicorn {
  server unix:/var/run/unicorn.sock;
}

server {
  listen   80;
  server_name localhost;

  access_log /var/log/nginx/application-access.log;
  error_log  /var/log/nginx/application-error.log;
  root /var/rails/public;
  index index.html;

  location / {
    proxy_set_header  X-Real-IP  $remote_addr;
    proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header  Host $http_host;
    proxy_redirect  off;

    if (!-f $request_filename) {
      proxy_pass http://unicorn;
    }
  }
}
EOF
)

echo "$NGINX_CONFIG" | sudo tee /etc/nginx/sites-available/default

# prepare rails app folder
if [ ! -e /var/rails ]; then
  echo "Creating rails application host folder"
  sudo ln -fs /vagrant /var/rails
fi

dpkg -s mongodb-10gen > /dev/null 2>&1 && {
  echo "mongodb is already installed"
} || {
  echo "mongodb is not installed"
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
  echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
  sudo apt-get update
  sudo apt-get install mongodb-10gen
}
