#!/usr/bin/env bash
APT_GET_UPDATE=0

function install() {
  for package in $@; do
    dpkg -s "$package" > /dev/null 2>&1 && {
      echo "$package is already installed"
    } || {
      echo "$package is not installed"
      if [[ $APT_GET_UPDATE -eq 0 ]]; then
        sudo apt-get update
        APT_GET_UPDATE=1
      fi

      sudo apt-get install "$package" -y
    }
  done
}

function gem_install() {
  to_install=()

  for gem_name in $@; do
    if [[ $(gem query -i -n $gem_name) == "true" ]]; then
      echo "gem $gem_name is already installed"
    else
      echo "gem $gem_name is not installed"
      to_install[${#to_install[@]}]=$gem_name
    fi
  done

  if [[ ${#to_install[@]} > 0 ]]; then
    gem install "$to_install"
  fi
}
