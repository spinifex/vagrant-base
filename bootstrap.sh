#!/usr/bin/env bash

VAGRANT_USER=vagrant
RUBY_TARGET_VERSION=2.1.0

source "/vagrant/bootstrap-lib.sh"

# does rvm need to be installed?
if which rvm > /dev/null 2>&1 ; then
  echo "rvm already installed"
else
  echo "rvm missing"
  install curl
  \curl -sSL https://get.rvm.io | bash
  usermod -a -G rvm $VAGRANT_USER

  # load RVM
  source /usr/local/rvm/scripts/rvm
fi

# does ruby need to be installed?
if [[ $(echo $RUBY_TARGET_VERSION | sed -e 's/-p/p/') == $(ruby -v | awk {'print $2'} | sed -e 's/p0//') ]]; then
  echo "Suitable ruby installed and set as default"
else
  echo "Default ruby is not the required version"
  if [[ $(rvm list) =~ "ruby-$RUBY_TARGET_VERSION" ]]; then
    echo "Desired version available"
  else
    echo "Missing required ruby version"
    install build-essential
    rvm install $RUBY_TARGET_VERSION
  fi

  rvm use $RUBY_TARGET_VERSION --default
fi

# install basic gems
gem_install god bundler

# runs the stack setup script if present
if [ -e /vagrant/stack-setup.sh ]; then 
  su $VAGRANT_USER -c /vagrant/stack-setup.sh
fi

# runs the application setup script if present
if [ -e /vagrant/application-setup.sh ]; then 
  su $VAGRANT_USER -c /vagrant/application-setup.sh
fi
